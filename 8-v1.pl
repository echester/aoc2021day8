use strict;
use warnings;
use List::Permutor;
use List::Util qw |first|;
use Time::HiRes qw |time|;
my $start = time();

sub unionsize($$) {
	# return integer that is the number of elements in first 
	# referenced array that are also present in the second
	# *there must be a better way to do this*
	my ($a, $b) = @_;
	my $c = 0;
	foreach my $i (@$a) {
		if (grep { $i eq $_ } @$b) {
			$c++;
		}
	}
	return $c;
}

sub sortElemAlpha($) {
	# replace strings within an array by alphabetically
	# sorted versions of themselves
	my $arr = shift;
	return map { join '',sort(split //) } @$arr;
}

# get input file name from command line
my $infile = $ARGV[0];
die "\ninput file not specified; borking.\n\n" unless defined($infile);

# defined digit mappings
my @digits = ('abcefg','cf','acdeg','acdfg','bcdf','abdfg','abdefg','acf','abcdefg','abcdfg');

# holders for signals string, values string, and each separated into elements 
my ($sig, $val);
my (@signals, @values);

# initialise accumulated total 
my $acc = 0;

# open the input file
open(my $inf, '<', $infile) || die "cannot open file $infile: $!\n";

# loop over input file
while(<$inf>) { 
	chomp; 
	($sig, $val) = split /\s+\|\s+/;

	# create a permutor over first 7 letters
	my $permutor = List::Permutor->new('A'..'G');
	# iterate over permutations
	while ( my @permutation = $permutor->next() ) {
		# remap signals using permute order
		my $newsig = $sig;
		# * this whole next nonsense must have a better solution *
		$newsig =~ s/a/$permutation[0]/g;
		$newsig =~ s/b/$permutation[1]/g;
		$newsig =~ s/c/$permutation[2]/g;
		$newsig =~ s/d/$permutation[3]/g;
		$newsig =~ s/e/$permutation[4]/g;
		$newsig =~ s/f/$permutation[5]/g;
		$newsig =~ s/g/$permutation[6]/g;
		@signals = split / /, lc $newsig;
		@signals = sortElemAlpha(\@signals);

		# get the number of signals that map onto digits using this permute
		# ** optimisation possible here, since we don't care about the value 
		# if it isn't 10: unionsize() should instead return boolean and should
		# do so as soon as it knows it won't be 10 **
		my $un = unionsize(\@signals, \@digits);

		# if we've reached all 10 digits...
		if ($un == 10) {
			# apply the same permute to the value string
			# look... that same hideous nonsense... I've not even made it a 
			# function. For shame. There's a way to do this using a map {} hash
			# but I haven't had time to figure it out.
			$val =~ s/a/$permutation[0]/g;
			$val =~ s/b/$permutation[1]/g;
			$val =~ s/c/$permutation[2]/g;
			$val =~ s/d/$permutation[3]/g;
			$val =~ s/e/$permutation[4]/g;
			$val =~ s/f/$permutation[5]/g;
			$val =~ s/g/$permutation[6]/g;    	
			@values = split / /, lc $val;
			@values = sortElemAlpha(\@values);

			# initialise real value for this line of input
			my $realval;
			# loop over each value string
			foreach my $v (@values) {
				# get the digit index where this value is found and append to the real value string
				$realval .= first { $digits[$_] eq $v } 0..$#digits;
			}
			# report value of this line; commented for speed
			# print "Value:\t$realval\n";
			# add this line's value to the accumulator
			$acc += $realval;
			# stop considering other permutes for this line
			last;
		}
	}
}

# endgame
close $inf;
printf "\nTotal = %d\n", $acc;
printf("\n== Execution Time: %0.02f s ==\n\n", time() - $start);
exit;
